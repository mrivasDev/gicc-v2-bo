# Project Title

NG - STARTER

### Installing

Install dependencies
 - npm i

Configure src/environments/environment.ts file

Run it
 - ng serve

## Useful commands

### To build
 - ng build
### To create a new component with module
 - ng g m path/NEW_COMPONENT
 - ng g c path/NEW_COMPONENT

### Don't know what to do?
 - ng help

## Deployment

No deployment so far

## Authors

* **Matias Rivas** - *Initial work* 

## License

No licence so far
