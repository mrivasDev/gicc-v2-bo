import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './core/auth.service';
import { HttpService } from './core/http.service';
import { UtilService } from './core/util.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {

	constructor(
		private httpService: HttpService,
		private router: Router,
		private utilService: UtilService,
		private authService: AuthService
	) { }

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return new Promise(resolve => {
			this.authService.identity().then(() => {
				resolve(true);
			}).catch(() => {
				this.utilService.notification('Su sesion ha expirado. Ingrese nuevamente.');
				this.router.navigateByUrl('/login');
				resolve(false);
			});
		})
	}
}
