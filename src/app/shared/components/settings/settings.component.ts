import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { UtilService } from '../../../core/util.service';
import { DashboardService } from '../../../pages/dashboard/dashboard.service';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss']
})


export class SettingsComponent implements OnInit {	
	
	themeColor: { value: 'primary' | 'accent' | 'warn', checked: true} = {value: 'primary', checked: true }; 
	
	isDarkTheme: boolean = false; 
	
	constructor(
		private dashboardService: DashboardService,
		private utilService: UtilService,
		private overlayContainer: OverlayContainer
		) {
			this.themeColor.value = this.utilService.getLS('themeColor', true);
			this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
		}
		
		ngOnInit(): void {
			
			this.dashboardService.onDarkThemeToggle.subscribe(() => {
				this.isDarkTheme = this.dashboardService.isDarkTheme;
			});
			
			this.dashboardService.onPrimaryThemeSelected.subscribe(() => {
				this.themeColor.value = 'primary';
			});	
			
		}
		
		toggleMode(): void {
			this.toggleTheme();
		}
		
		toggleTheme(): void {
			this.dashboardService.onDarkThemeToggle.emit();
			if (this.isDarkTheme) {
				this.overlayContainer.getContainerElement().classList.add('dark-theme');
			} else {
				this.overlayContainer
				.getContainerElement()
				.classList.remove('dark-theme');
			}
		}
		
		
	}
	