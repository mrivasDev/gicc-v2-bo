import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as dayjs from 'dayjs';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ConfirmOptions, ConfirmInput } from './interfaces';

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
    isHtml = false;
    confirmForm: FormGroup;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: ConfirmOptions,
        public matDialogRef: MatDialogRef<ConfirmDialogComponent>,
        private snackBar: MatSnackBar,
        private fb: FormBuilder
    ) {
        this.confirmForm = this.fb.group({});
        if (!this.data.title) {
            this.data.title = '¿Desea confirmar la acción?';
        }
        if (!this.data.cancel && this.data.cancel !== null) {
            this.data.cancel = 'Cancelar';
        }
        if (!this.data.confirm) {
            this.data.confirm = 'Confirmar';
        }
        if (this.data.text === undefined) {
            this.data.text = `Presione "${this.data.confirm}" para realizar la acción`;
        } else {
            this.isHtml = this.data.text && this.data.text.includes('</');
        }
        if (!this.data.inputs) {
            this.data.inputs = [];
        } else {
            for (let i = 0; i < this.data.inputs.length; i++) {
                const input = this.data.inputs[i];
                this.setDefaultValues(input);
                let formControl: FormControl = new FormControl(input.type === 'checkbox' ? false : null);
                this.confirmForm.addControl(input.name, formControl);

                if (input.type === 'select') {
                    formControl = new FormControl(null);
                    this.confirmForm.addControl(input.name + 'Select', formControl);
                }

                if (input.condition && input.validations) {
                    this.confirmForm.valueChanges.subscribe(formValue => {
                        if (input.condition(this.confirmForm)) {
                            formControl.setValidators(input.validations);
                        } else {
                            formControl.clearValidators();
                        }
                    });
                } else if (input.requiredIf) {
                    this.confirmForm.valueChanges.subscribe(formValue => {
                        if (input.requiredIf(this.confirmForm)) {
                            formControl.setValidators(Validators.required);
                        } else {
                            formControl.clearValidators();
                        }
                    });
                } else if (input.validations) {
                    formControl.setValidators(input.validations);
                }

                if (input.type !== 'select' && input.type !== 'date' && input.type !== 'checkbox') {
                    this.confirmForm.get(input.name).setValue('');
                } else if (input.type === 'date') {
                    this.confirmForm.get(input.name).setValue(dayjs());
                }
            }
        }
    }

    setDefaultValues(input: ConfirmInput) {
        if (input.type === 'select') {
            input.selectOptions.label = input.selectOptions.label ? input.selectOptions.label : 'descripcion';
            input.selectOptions.options = input.selectOptions.options ? input.selectOptions.options : [];
            input.selectOptions.search = input.selectOptions.search ? input.selectOptions.search : '';
        }
        input.condition = input.condition ? input.condition : form => true;
    }

    onSelect(input: ConfirmInput, res: any) {
        this.confirmForm.get(input.name).setValue(res);
    }

    closeDialog(confirma: boolean) {
        this.confirmForm.markAsDirty();
        this.confirmForm.markAsTouched();
        this.confirmForm.updateValueAndValidity();
        for (const key in this.confirmForm.controls) {
            if (this.confirmForm.controls.hasOwnProperty(key)) {
                this.confirmForm.controls[key].updateValueAndValidity();
            }
        }
        if (confirma) {
            if (this.confirmForm.invalid) {
                this.snackBar.open('Error de validación. Revise los campos', 'Cerrar', {
                    duration: 5000,
                    panelClass: 'snack',
                    verticalPosition: 'top',
                    horizontalPosition: 'end'
                });
                return;
            }
            const value = {};
            for (const key in this.confirmForm.value) {
                if (this.confirmForm.value.hasOwnProperty(key) && !key.includes('Select')) {
                    value[key] = this.confirmForm.value[key];
                }
            }
            this.matDialogRef.close(value);
        } else {
            this.matDialogRef.close(null);
        }
    }
}
