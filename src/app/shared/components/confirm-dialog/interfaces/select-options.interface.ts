export interface SelectOptions {
    options?: any[];
    search?: string;
    label?: string;
}
