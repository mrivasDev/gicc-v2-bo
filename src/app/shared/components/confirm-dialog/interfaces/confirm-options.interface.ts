import { ConfirmInput } from './confirm-input.interface';

export interface ConfirmOptions {
    title?: string;
    text?: string;

    /** Button labels */
    confirm?: string;
    cancel?: string;

    inputs?: ConfirmInput[];
}
