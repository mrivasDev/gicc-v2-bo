import { ValidatorFn, FormGroup } from '@angular/forms';
import { SelectOptions } from './select-options.interface';
import * as dayjs from 'dayjs';

export type InputType = 'select' | 'text' | 'textarea' | 'date' | 'file' | 'checkbox';

export interface ConfirmInput {
    type: InputType;
    name: string;
    placeholder: string;
    required?: boolean;
    requiredIf?: (form: FormGroup) => boolean;

    /** Reactive forms validation */
    validations?: ValidatorFn[];

    /** Conditionally show input */
    condition?: (form: FormGroup) => boolean;

    /** Options for 'select' type  */
    selectOptions?: SelectOptions;

    /** Document type ID for 'file' type */
    fileTypeId?: number;

    /** Max length for 'textarea' type */
    max?: number;

    /** Mínimo y máximo para fecha */
    minDate?: dayjs.Dayjs;
    maxDate?: dayjs.Dayjs;
}
