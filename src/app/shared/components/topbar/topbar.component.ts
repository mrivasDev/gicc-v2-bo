import { Component, OnInit } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { UtilService } from '../../../core';
import { DashboardService } from '../../../pages/dashboard';
import { HostListener } from '@angular/core';
import { User } from '../../../models';
import { AuthService } from '../../../core/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-topbar',
	templateUrl: './topbar.component.html',
	styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
	themeColor: 'primary' | 'accent' | 'warn' = 'primary'; 
	
	isDarkTheme: boolean = false; 
	
	user: User = this.utilService.getLS('user', true);
	
	pagina: string = '';
	
	isSnavOpened: boolean = false;
	
	isConfNavOpened: boolean = false;
	
	isMobile: boolean = false;
	
	constructor(
		private dashboardService: DashboardService,
		private utilService: UtilService,
		private authService: AuthService,
		private router: Router,
		private overlayContainer: OverlayContainer) {
			this.themeColor = this.utilService.getLS('themeColor', true);
			this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
		}
		
		ngOnInit(): void {

			if (document.documentElement.clientWidth <= 750) {
				this.isMobile = true;
			}

			this.dashboardService.onSidenavToggle.subscribe(() => {
				this.isSnavOpened = this.dashboardService.isSnavOpened;
			});
			
			this.dashboardService.onConfNavToggle.subscribe(() => {
				this.isConfNavOpened = this.dashboardService.isConfNavOpened;
			});
			
			this.dashboardService.onDarkThemeToggle.subscribe(() => {
				this.isDarkTheme = this.dashboardService.isDarkTheme;
			});
			
			this.dashboardService.breadcrumbs.subscribe((pagina: string) => {
				this.pagina = pagina;
			});
			
			this.dashboardService.onPrimaryThemeSelected.subscribe(() => {
				this.themeColor = 'primary';
			});	
			
			this.dashboardService.onWarnThemeSelected.subscribe(() => {
				this.themeColor = 'warn';
			});
			
			this.dashboardService.onAccentThemeSelected.subscribe(() => {
				this.themeColor = 'accent';
			});
		}

		toggleSidenav() {
			this.dashboardService.onSidenavToggle.emit();
		}
		
		toggleConfNav() {
			this.dashboardService.onConfNavToggle.emit();
		}
		
		logout() {
			sessionStorage.clear();
			this.utilService.setLS("user", null, true);
			this.authService.logout();
			this.router.navigate(['/login']);
		}
		
		@HostListener('window:resize', ['$event'])
		onResize(event) {
			this.isMobile = event.target.innerWidth < 750;
		}
	}
	