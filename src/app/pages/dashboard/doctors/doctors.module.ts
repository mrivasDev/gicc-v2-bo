import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorsComponent } from './doctors.component';
import { RouterModule } from '@angular/router';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';
import { PictureDialog } from './PictureDialog/pictureDialog.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [DoctorsComponent, PictureDialog],
  imports: [
    CommonModule,
    HeaderModule,
    SegmentModule,
    TableModule,
    SharedModule,
    ImageCropperModule,
    RouterModule.forChild([{ path: '', component: DoctorsComponent }])
  ]
})
export class DoctorsModule { }
