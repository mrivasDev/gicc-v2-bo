import { HostListener } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { FormObject } from 'projects/ng-utils/src/lib/header/header.component';
import { HttpService } from '../../../core/http.service';
import { UtilService } from '../../../core';
import { ActionTable } from 'projects/ng-utils/src/lib/table/table.component';
import { MatDialog } from '@angular/material/dialog';
import { PictureDialog } from './PictureDialog/pictureDialog.component';

@Component({
    selector: 'app-doctors',
    templateUrl: './doctors.component.html',
    styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

    public dataSource: any[] = [];

    public isLoading: boolean = false;

    public pageSize: number = 0;

    public pageIndex: number = 0;

    public resultsLength: number = 0;

    public pageSizeOptions: number[] = [15];

    public columnsToDisplay: { field: string, display: string }[] = [
        { field: "name", display: "Nombre" },
        { field: "image", display: "Foto" },
        { field: "lastname", display: "Apellido" },
        { field: "specialty", display: "Especialidad" },
        { field: "enrollment", display: "Matricula" },
        { field: "actions", display: "Acciones" },
    ];

    public isMobile: boolean = false;

    public modalTitle: string = 'Alta profesional';

    public modalSubTitle: string = 'Dar de alta a un nuevo profesional';

    public modalApiUrl: string = 'doctors';

    public modalForm: FormObject[] = [
        { field: 'name', display: 'Nombre', type: 'text', required: true, element: 'input', placeholder: 'Nombre' },
        { field: 'lastname', display: 'Apellido', type: 'text', required: true, element: 'input', placeholder: 'Apellido' },
        { field: 'specialty', display: 'Especialidad', type: 'text', required: true, element: 'input', placeholder: 'Especialidad' },
        { field: 'enrollment', display: 'Matricula', type: 'number', required: true, element: 'input', placeholder: 'Matricula', min: 1, max: 10 }
    ];

    public tableActions: ActionTable[] = [
        {
            icon: 'account_circle',
            name: 'Agregar imagen',
            callback: (element) => { this.openUploadImageDialog(element) },
        },
        {
            icon: 'delete',
            name: 'Eliminar',
            callback: (element) => { console.log('Eliminar') },
        },
    ];

    constructor(public dialog: MatDialog, private httpService: HttpService, private utilService: UtilService) { }

    openUploadImageDialog(element) {
        const dialogRef = this.dialog.open(PictureDialog, {
            width: '650px',
            height: 'auto',
            data: {
                element
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }

    ngOnInit(): void {
        this.isMobile = document.documentElement.clientWidth <= 750;
        this.fetchDoctors();
    }

    fetchDoctors(searchText: string | number = ""): void {
        this.isLoading = true;
        this.httpService.get('doctors', { size: this.pageSize, page: this.pageIndex, searchText }).then(({
            per_page,
            current_page,
            total,
            data }: any) => {

            this.pageSize = per_page;

            this.pageIndex = current_page - 1;

            this.resultsLength = total;

            this.dataSource = data;

            this.isLoading = false;
        });
    }

    handlePageEvent(event: PageEvent) {
        if (event.pageSize !== this.pageSize) {
            this.pageSize = event.pageSize;
        }

        if (event.pageIndex !== this.pageIndex) {
            this.pageIndex = event.pageIndex;
        }

        this.fetchDoctors();
    }

    handleFinishCreating(event: any) {
        this.fetchDoctors();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event): void {
        this.isMobile = event.target.innerWidth < 750;
    }
}
