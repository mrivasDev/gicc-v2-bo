import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService, UtilService } from '../../../../core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
@Component({
    selector: 'dialog-doctor-picture-dialog',
    templateUrl: 'pictureDialog.component.html',
    styleUrls: ['./pictureDialog.component.scss']
})
export class PictureDialog {

    public doctor: any = null;

    public imageChangedEvent: any = '';

    public croppedImage: any = '';

    public file: File = null;

    public editionMode: boolean = true;

    constructor(public dialogRef: MatDialogRef<PictureDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private httpService: HttpService,
        private utilService: UtilService
    ) {
        this.doctor = data.element;
    }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }

    imageCropped(event: ImageCroppedEvent) {

        this.editionMode = true;

        this.croppedImage = event.base64;

        this.file = this.utilService.base64ToFile(
            event.base64,
            this.imageChangedEvent.target.files[0].name,
        );

    }

    loadImageFailed() {
        this.utilService.notification('El formato de la imagen no es correcto. Recuerde usar PNG/JPG');
        // show message
    }

    onSubmit(event: any) {
        const formData = new FormData();
        formData.append('file', this.file);
        this.httpService.post('upload/image/public/doctors', formData).then((resp: any) => {
            if (resp.id) {
                const newDoctor = { ...this.doctor, ...{ image_id: resp.id } };
                this.httpService.put(`doctors/${this.doctor.id}`, newDoctor).then((resp: any) => {
                    this.utilService.notification('La imagen se ha subido exitosamente.');
                    this.close();
                })
            }
        }).catch(error => {
            console.log('Hubo un error al subir la imagen');
        });

    }

    public close(): void {
        this.dialogRef.close();
    }
}
