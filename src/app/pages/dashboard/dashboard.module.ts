import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { MatRippleModule } from '@angular/material/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { TopbarComponent } from 'src/app/shared/components/topbar/topbar.component';
import { LayoutComponent } from 'src/app/shared/components/layout/layout.component';
import { SettingsComponent } from 'src/app/shared/components/settings/settings.component';

@NgModule({
	declarations: [
		DashboardComponent,
		LayoutComponent,
		TopbarComponent,
		SettingsComponent
	],
	imports: [
		CommonModule,
		DashboardRoutingModule,
		SharedModule,
		MatRippleModule
	],
	providers: [
		DashboardService
	]
})
export class DashboardModule { }
