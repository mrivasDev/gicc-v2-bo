import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';

@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    HeaderModule,
    SegmentModule,
    TableModule,
    RouterModule.forChild([{ path: '', component: UsersComponent }])
  ]
})
export class UsersModule { }
