import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { HttpService, UtilService } from '../../../core';
import { FormObject } from 'projects/ng-utils/src/lib/header/header.component';
import { ActionTable } from 'projects/ng-utils/src/lib/table/table.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public dataSource: any[] = [];

  public isLoading: boolean = false;

  public columnsToDisplay: { field: string, display: string }[] = [
    { field: "name", display: "Nombre" },
    { field: "surname", display: "Apellido" },
    { field: "email", display: "Email" },
    { field: "dni", display: "DNI" },
    { field: "actions", display: "Acciones" },
  ];

  public isMobile: boolean = false;

  public modalTitle: string = 'Alta usuario';

  public modalSubTitle: string = 'Dar de alta a un usuario';

  public modalApiUrl: string = 'users';

  public modalForm: FormObject[] = [
    { field: 'name', display: 'Nombre', type: 'text', required: true, element: 'input', placeholder: 'Nombre' },
    { field: 'surname', display: 'Apellido', required: true, element: 'input', placeholder: 'Apellido' },
    { field: 'email', display: 'Email', type: 'email', required: true, element: 'input', placeholder: 'Email' },
    { field: 'dni', display: 'DNI', type: 'number', required: true, element: 'input', placeholder: 'DNI' }
  ];

  public pageSize: number = 0;

  public pageIndex: number = 0;

  public resultsLength: number = 0;

  public pageSizeOptions: number[] = [15];

  public tableActions: ActionTable[] = [
    {
      icon: 'edit',
      name: 'Editar',
      callback: (element) => { this.openEditDialog(element) },
    },
    {
      icon: 'delete',
      name: 'Eliminar',
      callback: (element) => { this.handleDelete(element) },
    },
  ];


  constructor(public dialog: MatDialog, private httpService: HttpService, private utilService: UtilService) { }

  ngOnInit(): void {
    this.isMobile = document.documentElement.clientWidth <= 750;
    this.fetchUsers();

  }

  fetchUsers(searchText: string | number = ""): void {
    this.isLoading = true;
    this.httpService.get('users', { size: this.pageSize, page: this.pageIndex, searchText }).then(({
      per_page,
      current_page,
      total,
      data }: any) => {

      this.pageSize = per_page;

      this.pageIndex = current_page - 1;

      this.resultsLength = total;

      this.dataSource = data;

      this.isLoading = false;
    });
  }


  handlePageEvent(event: PageEvent) {
    if (event.pageSize !== this.pageSize) {
      this.pageSize = event.pageSize;
    }

    if (event.pageIndex !== this.pageIndex) {
      this.pageIndex = event.pageIndex;
    }

    this.fetchUsers();
  }

  handleFinishCreating(event: any) {
    this.fetchUsers();
  }

  openEditDialog(element) {
    // const dialogRef = this.dialog.open(NewsEditDialog, {
    //   width: '650px',
    //   height: 'auto',
    //   data: {
    //     element
    //   }
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }

  handleDelete(element) {
    console.log('delete');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.isMobile = event.target.innerWidth < 750;
  }
}
