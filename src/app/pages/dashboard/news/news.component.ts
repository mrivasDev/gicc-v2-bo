import { PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { Component, HostListener, OnInit } from '@angular/core';
import { HttpService, UtilService } from '../../../core';
import { FormObject } from 'projects/ng-utils/src/lib/header/header.component';
import { ActionTable } from 'projects/ng-utils/src/lib/table/table.component';
import { NewsEditDialog } from './news-edit-dialog/news-edit-dialog.component';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

    public dataSource: any[] = [];
    
    public isLoading: boolean = false;
    
    public columnsToDisplay: { field: string, display: string }[] = [
        { field: "title", display: "Titulo" },
        { field: "body", display: "Cuerpo" },
        { field: "listada", display: "Listar" },
        { field: "actions", display: "Acciones" },
    ];


    public isMobile: boolean = false;
    
    public modalTitle: string = 'Alta noticia';
    
    public modalSubTitle: string = 'Dar de alta a una noticia';
    
    public modalApiUrl: string = 'news';
    
    public modalForm: FormObject[] = [
        { field: 'title', display: 'Titulo', type: 'text', required: true, element: 'input', placeholder: 'Titulo' },
        { field: 'body', display: 'Cuerpo', required: true, element: 'textarea', placeholder: 'Cuerpo' },
        { field: 'listed', display: 'Mostrar', type: 'boolean', required: true, element: 'checkbox', placeholder: 'Mostrar' }
    ];

    public pageSize: number = 0;

    public pageIndex: number = 0;

    public resultsLength: number = 0;

    public pageSizeOptions: number[] = [15];

    public tableActions: ActionTable[] = [
        {
            icon: 'edit',
            name: 'Editar',
            callback: (element) => { this.openEditDialog(element) },
        },
        {
            icon: 'delete',
            name: 'Eliminar',
            callback: (element) => { this.handleDelete(element) },
        },
    ];


    constructor(public dialog: MatDialog, private httpService: HttpService, private utilService: UtilService) { }

    ngOnInit(): void {
        this.isMobile = document.documentElement.clientWidth <= 750;
        this.fetchNews();

    }

    fetchNews(searchText: string | number = ""): void {
        this.isLoading = true;
        this.httpService.get('news', { size: this.pageSize, page: this.pageIndex, searchText }).then(({
            per_page,
            current_page,
            total,
            data }: any) => {

            this.pageSize = per_page;

            this.pageIndex = current_page - 1;

            this.resultsLength = total;

            this.dataSource = data.map(news => {
                news.listada = news.listed ? 'Listar' : 'No listar';
                return news;
            });

            this.isLoading = false;
        });
    }


    handlePageEvent(event: PageEvent) {
        if (event.pageSize !== this.pageSize) {
            this.pageSize = event.pageSize;
        }

        if (event.pageIndex !== this.pageIndex) {
            this.pageIndex = event.pageIndex;
        }

        this.fetchNews();
    }

    handleFinishCreating(event: any) {
        this.fetchNews();
    }

    openEditDialog(element) {
        const dialogRef = this.dialog.open(NewsEditDialog, {
            width: '650px',
            height: 'auto',
            data: {
                element
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }

    handleDelete(element) {
        console.log('delete');
    }

    @HostListener('window:resize', ['$event'])
    onResize(event): void {
        this.isMobile = event.target.innerWidth < 750;
    }

}
