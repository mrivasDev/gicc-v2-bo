import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news.component';
import { RouterModule } from '@angular/router';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';
import { NewsEditDialog } from './news-edit-dialog/news-edit-dialog.component';

@NgModule({
  declarations: [NewsComponent, NewsEditDialog],
  imports: [
    CommonModule,
    HeaderModule,
    SegmentModule,
    TableModule,
    RouterModule.forChild([{ path: '', component: NewsComponent }])
  ]
})
export class NewsModule { }
