import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService, UtilService } from '../../../../core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
@Component({
    selector: 'dialog-news-adit',
    templateUrl: 'news-edit-dialog.component.html',
    styleUrls: ['./news-edit-dialog.component.scss']
})
export class NewsEditDialog {

    public news: any = null;

    constructor(public dialogRef: MatDialogRef<NewsEditDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private httpService: HttpService,
        private utilService: UtilService
    ) {
        this.news = data.news;
    }

  

    onSubmit(event: any) {
       
    }

    public close(): void {
        this.dialogRef.close();
    }
}
