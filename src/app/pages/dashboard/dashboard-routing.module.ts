import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DoctorsModule } from './doctors/doctors.module';
import { HomeModule } from './home/home.module';
import { NewsModule } from './news/news.module';
import { RoleGuard } from './role.guard';
import { UsersModule } from './users/users.module';

const routes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		children: [
			{
				path: '',
				redirectTo: 'inicio',
				pathMatch: 'full'
			},
			{
				path: 'inicio',
				loadChildren: () => HomeModule,
				canActivate: [RoleGuard],
				
			}
			,
			{
				path: 'profesionales',
				loadChildren: () => DoctorsModule,
				canActivate: [RoleGuard],
				
			}
			,
			{
				path: 'noticias',
				loadChildren: () => NewsModule,
				canActivate: [RoleGuard],
				
			}
			,
			{
				path: 'usuarios',
				loadChildren: () => UsersModule,
				canActivate: [RoleGuard],
				
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DashboardRoutingModule { }
