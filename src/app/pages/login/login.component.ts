import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilService } from '../../core'
import { AuthService } from '../../core/auth.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	
	constructor	
	(		
		private router: Router,
		private utilService: UtilService,
		private authService: AuthService
		) { }
		loading = false;
		
		loginForm = new FormGroup({
			username: new FormControl(null),
			password: new FormControl(null),
		});
		
		showPass = false;
		
		ngOnInit(): void {
		}
		reset() {
			this.loginForm.reset();
		}
		
		loginDisabled() {
			return !this.loginForm.value.username || !this.loginForm.value.password || this.loading
		}
		
		login() {
			const login = this.loginForm.value;
			this.loading = true;
			const notification = this.utilService.notification("Login in...", "", null );
				this.authService
				.login(login.username, login.password)
				.then((data) => {
					if (data && data.user) {
						this.utilService.setLS("user", data.user, true);
						this.utilService.saveAuth(data);
						this.router
						.navigateByUrl("/")
						.then(() => {
							notification.dismiss();
							this.loading = false;
						});
					}
				})
				.catch((error) => {
					this.utilService.notification(error.message);
					this.loading = false;
					
				});
			}
			
		}
		