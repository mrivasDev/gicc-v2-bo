import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { ButtonModule } from '../button/button.module';
import { SegmentModule } from '../segment/segment.module';
import { LoadingModule } from '../loading/loading.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';


@NgModule({
    declarations: [
        TableComponent,
    ],
    imports: [
        CommonModule,
        MatCardModule,
        MatIconModule,
        ButtonModule,
        SegmentModule,
        LoadingModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatRippleModule,
        MatFormFieldModule,
        MatCardModule,
        MatIconModule,
        MatMenuModule,
        MatExpansionModule,
        MatTableModule,
        MatInputModule,
        MatPaginatorModule
    ],
    exports: [
        TableComponent
    ]
})
export class TableModule { }

