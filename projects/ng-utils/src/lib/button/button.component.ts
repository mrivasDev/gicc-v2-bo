import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ng-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() primary = false;

  @Input() label = 'Button';

  @Output() onClick = new EventEmitter<Event>();

  constructor() { }
  ngOnInit(): void {
  }

}
