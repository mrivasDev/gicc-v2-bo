import { Component } from '@angular/core';
import { EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ng-segment',
  templateUrl: './segment.component.html',
  styleUrls: ['./segment.component.scss']
})
export class SegmentComponent{
  
  @Input() icon: string = '';
  @Input() message: string = '';
  @Input() actionMessage: string = '';
  @Input() actionLabel: string = '';

  @Output() actionEvent = new EventEmitter<Event>();


  constructor() { }

}
