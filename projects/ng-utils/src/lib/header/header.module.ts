import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { ButtonModule } from '../button/button.module';
import { LoadingModule } from '../loading/loading.module';
import { MainActionModalModule } from '../mainActionModal/mainActionModal.module';
import { MatDividerModule } from '@angular/material/divider';
import { MainActionBottomSheetModule } from '../mainActionBottomSheet/mainActionBottomSheet.module';

@NgModule({
    declarations: [
        HeaderComponent
    ],
    imports: [
        CommonModule,
        ButtonModule,
        LoadingModule,
        MatDividerModule,
        MainActionModalModule,
        MainActionBottomSheetModule,
    ],
    exports: [
        HeaderComponent
    ]
})
export class HeaderModule { }
