import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MainActionModalComponent } from '../mainActionModal/mainActionModal.component';
import { MainActionBottomSheetComponent } from '../mainActionBottomSheet/mainActionBottomSheet.component';

export interface FormObject {
  field: string,
  display: string,
  type?: string,
  required: boolean,
  element: 'input' | 'textarea' | 'datepicker' | 'checkbox' | 'select' | 'radio' | 'file',
  placeholder: string,
  options?: any[],
  min?: number,
  max?: number
}


@Component({
  selector: 'ng-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() icon: string = '';
  @Input() title: string = 'Title of the screen';
  @Input() subtitle: string = 'SubTitle';

  @Input() titleClass: string = '';
  @Input() subtitleClass: string = '';

  @Input() mainActionLabel: string = '';
  @Input() secondaryActionLabel: string = '';

  @Output() mainActionEvent = new EventEmitter<Event>();
  @Output() secondaryActionEvent = new EventEmitter<Event>();

  @Input() modalTitle: string = '';
  @Input() modalSubTitle: string = '';

  @Input() modalApiUrl: string = '';
  @Input() modalForm: FormObject[] = [];
  @Input() method: string = 'post';

  @Input() mobile: boolean = false;

  @Output() actionFinished = new EventEmitter<Event>();

  constructor(
    public mainActionDialog: MatDialog,
    private mainActionBottomSheet: MatBottomSheet,
  ) { }

  ngOnInit(): void { }

  mainClickHandler() {
    if (this.mobile) {
      this.openBottomSheet();
    } else {
      console.log('voy aca');
      this.openDialog();
    }
  }

  openDialog(): void {
    const dialogRef = this.mainActionDialog.open(MainActionModalComponent, {
      width: '650px',
      height: 'auto',
      data: {
        title: this.modalTitle,
        subtitle: this.modalSubTitle,
        apiUrl: this.modalApiUrl,
        form: this.modalForm,
        method: this.method
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.actionFinished.emit();
    });
  }

  openBottomSheet(): void {
    const bottomSheet = this.mainActionBottomSheet.open(MainActionBottomSheetComponent, {
      data: {
        title: this.modalTitle,
        subtitle: this.modalSubTitle,
        apiUrl: this.modalApiUrl,
        form: this.modalForm,
        method: this.method
      }
    });
    bottomSheet.afterDismissed().subscribe(() => {
      this.actionFinished.emit();
    });
  }


}

